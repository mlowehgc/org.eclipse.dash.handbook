<?xml version="1.0" encoding="UTF-8"?>

<!--
 * Copyright (C) 2015 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: DOCUMENTATION
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
-->
 
<setup:Project
    xmi:version="2.0"
    xmlns:xmi="http://www.omg.org/XMI"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:git="http://www.eclipse.org/oomph/setup/git/1.0"
    xmlns:projects="http://www.eclipse.org/oomph/setup/projects/1.0"
    xmlns:setup="http://www.eclipse.org/oomph/setup/1.0"
    xmlns:setup.p2="http://www.eclipse.org/oomph/setup/p2/1.0"
    xsi:schemaLocation="http://www.eclipse.org/oomph/setup/git/1.0 http://git.eclipse.org/c/oomph/org.eclipse.oomph.git/plain/setups/models/Git.ecore http://www.eclipse.org/oomph/setup/projects/1.0 http://git.eclipse.org/c/oomph/org.eclipse.oomph.git/plain/setups/models/Projects.ecore"
    name="dash.handbook"
    label="Dash Handbook">
  <setupTask
      xsi:type="setup:EclipseIniTask"
      option="-Xmx"
      value="1024m"
      vm="true">
    <description>Set the heap space needed to work with the projects of ${scope.project.label}</description>
  </setupTask>
  <setupTask
      xsi:type="setup.p2:P2Task">
    <requirement
        name="org.eclipse.gef4.dot.sdk.feature.group"/>
    <repository
        url="http://download.eclipse.org/tools/gef/gef4/updates/releases"/>
  </setupTask>
  <setupTask
      xsi:type="setup.p2:P2Task">
    <requirement
        name="org.eclipse.mylyn.wikitext.asciidoc"/>
    <requirement
        name="org.eclipse.mylyn.wikitext.asciidoc.ui"/>
    <requirement
        name="org.eclipse.mylyn.docs.epub.feature.group"/>
    <repository
        url="http://download.eclipse.org/mylyn/snapshots/nightly/docs/"/>
  </setupTask>
  <setupTask
      xsi:type="git:GitCloneTask"
      id="dash.handbook.git.clone"
      remoteURI="dash/org.eclipse.dash.handbook">
    <annotation
        source="http://www.eclipse.org/oomph/setup/InducedChoices">
      <detail
          key="inherit">
        <value>eclipse.git.gerrit.remoteURIs</value>
      </detail>
      <detail
          key="label">
        <value>${scope.project.label} Git or Gerrit repository</value>
      </detail>
      <detail
          key="target">
        <value>remoteURI</value>
      </detail>
    </annotation>
    <description>${scope.project.label}</description>
  </setupTask>
  <setupTask
      xsi:type="projects:ProjectsImportTask">
    <sourceLocator
        rootFolder="${dash.handbook.git.clone.location}"/>
  </setupTask>
  <stream name="master"
      label="Master"/>
  <logicalProjectContainer
      xsi:type="setup:ProjectCatalog"
      href="index:/org.eclipse.setup#//@projectCatalogs[name='org.eclipse']"/>
  <description>Dash Handbook contains the documentation sources for the Eclipse fundation Handbooks.				</description>
</setup:Project>
