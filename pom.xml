<!--
 * Copyright (C) 2015,2021 Eclipse Foundation, Inc. and others. 
 * 
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 * 
 * SPDX-FileType: DOCUMENTATION
 *
 * SPDX-FileCopyrightText: 2015 Eclipse Foundation, Inc.
 * SPDX-FileCopyrightText: 2015 Contributors to the Eclipse Foundation
 *
 * SPDX-License-Identifier: EPL-2.0
-->

<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<name>Eclipse Foundation Project Handbook</name>
	<organization>
		<name>Eclipse Dash Project</name>
	</organization>

	<groupId>org.eclipse.dash</groupId>
	<artifactId>org.eclipse.dash.handbook</artifactId>
	<version>2022.03.18</version>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<asciidoctor.maven.plugin.version>2.1.0</asciidoctor.maven.plugin.version>
		<asciidoctorj.version>2.4.1</asciidoctorj.version>
		<asciidoctorj.diagram.version>2.0.2</asciidoctorj.diagram.version>
		<asciidoctorj.pdf.version>1.5.3</asciidoctorj.pdf.version>
		<jruby.version>9.2.13.0</jruby.version>
	</properties>

	<build>
		<defaultGoal>process-resources</defaultGoal>
		<plugins>
			<plugin>
				<groupId>org.asciidoctor</groupId>
				<artifactId>asciidoctor-maven-plugin</artifactId>
				<version>${asciidoctor.maven.plugin.version}</version>
				<dependencies>
					<dependency>
						<groupId>org.jruby</groupId>
						<artifactId>jruby-complete</artifactId>
						<version>${jruby.version}</version>
					</dependency>
					<dependency>
						<groupId>org.asciidoctor</groupId>
						<artifactId>asciidoctorj</artifactId>
						<version>${asciidoctorj.version}</version>
					</dependency>
					<dependency>
						<groupId>org.asciidoctor</groupId>
						<artifactId>asciidoctorj-diagram</artifactId>
						<version>${asciidoctorj.diagram.version}</version>
					</dependency>
					<dependency>
						<groupId>org.asciidoctor</groupId>
						<artifactId>asciidoctorj-pdf</artifactId>
						<version>${asciidoctorj.pdf.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<sourceDirectory>source</sourceDirectory>
					<requires>
						<require>asciidoctor-diagram</require>
					</requires>
					<backend>xhtml5</backend>
					<sourceHighlighter>coderay</sourceHighlighter>
					<attributes>
						<endpoint-url>http://www.eclipse.org</endpoint-url>
						<project-version>${project.version}</project-version>
						<doctype>book</doctype>
						<imagesoutdir>.</imagesoutdir>
						<imagesdir>.</imagesdir>
						<data-uri />
						<toc>left</toc>
						<icons />
						<sectanchors>true</sectanchors>
						<idprefix />
						<idseparator>-</idseparator>
						<docinfo1>true</docinfo1>
						<embedAssets>true</embedAssets>
						<stylesheet>handbook.css</stylesheet>
					</attributes>
				</configuration>
				<executions>
					<execution>
						<id>asciidoc-to-html-eclipse</id>
						<phase>generate-resources</phase>
						<goals>
							<goal>process-asciidoc</goal>
						</goals>
						<configuration>
							<sourceDocumentName>eclipse.adoc</sourceDocumentName>
							<headerFooter>false</headerFooter>
							<attributes>
							</attributes>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.asciidoctor
										</groupId>
										<artifactId>
											asciidoctor-maven-plugin
										</artifactId>
										<versionRange>
											[1.5.3,)
										</versionRange>
										<goals>
											<goal>
												process-asciidoc
											</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore></ignore>
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
</project>